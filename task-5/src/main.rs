use std::fs;
use std::io::prelude::*;
use std::io;
use std::collections::HashMap;

fn parse_single(s: &str) -> u32 {
    let bin_s = s.replace("B", "1").replace("F", "0")
                 .replace("R", "1").replace("L", "0");
    println!("{} {} {}", s, bin_s, u32::from_str_radix(&bin_s, 2).unwrap());
    u32::from_str_radix(&bin_s, 2).unwrap()
}

fn parse_examples(input: String) -> Vec<u32> {
    let v = input.split("\n")
                 .map(parse_single).collect();
    v
}

fn main() -> io::Result<()> {
    let raw_data = fs::read_to_string("data.txt").unwrap();
    let examples = parse_examples(raw_data);
    let min: u32 = *examples.iter().min().unwrap();
    let max: u32 = *examples.iter().max().unwrap();
    let mut i: u32 = min + 1;
    while i < (max - 1) {
        if !examples.contains(&i) && examples.contains(&(i+1)) && examples.contains(&(i-1)) {
            break;
        }
        i += 1;
    }
    println!("Total num: {}", i);
    Ok(())
}
