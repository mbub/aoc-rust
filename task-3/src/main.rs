use std::fs;
use std::io::prelude::*;
use std::io;
use std::collections::HashMap;


fn parse_single(s: &str) -> io::Result<Vec<char>> {
    let mut ret: Vec<char> = s.chars().collect();
    Ok(ret)
}

fn parse_examples(input: String) -> io::Result<Vec<Vec<char>>> {
    let v = input.split("\n")
                 .map(parse_single)
                 .map(|x| x.unwrap())
                 .collect::<Vec<Vec<char>>>();
    Ok(v)
}

fn main() -> io::Result<()> {
    let raw_data = fs::read_to_string("data.txt").unwrap();
    let map = parse_examples(raw_data)?;
    let mut ans: u64 = 0;
    let mut x = 0;
    let mut ans2: u64 = 1;
    for r in &map {
        if (x as usize) >= r.len() {
            x -= r.len();
        }
        ans += (r[x] == '#') as u64;
        x += 3;
    }
    ans2 *= ans;
    ans = 0;
    x = 0;
    for r in &map {
        if (x as usize) >= r.len() {
            x -= r.len();
        }
        ans += (r[x] == '#') as u64;
        x += 1;
    }
    ans2 *= ans;
    ans = 0;
    x = 0;
    for r in &map {
        if (x as usize) >= r.len() {
            x -= r.len();
        }
        ans += (r[x] == '#') as u64;
        x += 5;
    }
    ans2 *= ans;
    ans = 0;
    x = 0;
    for r in &map {
        if (x as usize) >= r.len() {
            x -= r.len();
        }
        ans += (r[x] == '#') as u64;
        x += 7;
    }
    ans2 *= ans;
    ans = 0;
    x = 0;
    for i in 0..map.len() {
        if i % 2 == 1 {
            continue;
        }
        println!("{} {}", x, i);
        if (x as usize) >= map[0].len() {
            x -= map[0].len();
        }
        ans += (map[i][x] == '#') as u64;
        x += 1;
    }
    ans2 *= ans;
    
    println!("Total num: {}", ans2);
    Ok(())
}
