use std::fs;
use std::io::prelude::*;

fn func(s: &str) -> i32{
    if (s.len() > 0) {
        s.parse::<i32>().unwrap()
    } else {
        -1
    }
}

fn input_parse(input: String) -> Vec<i32> {
    let v = input.split("\n")
                 .map(func)
                 .filter(|x| x >= &0)
                 .collect::<Vec<i32>>();
    v
}

fn main() {
    let raw_data = fs::read_to_string("data.txt").unwrap();
    let data = input_parse(raw_data);
    for x in data.clone() {
        for y in data.clone() {
            for z in data.clone() {
                if x + y + z == 2020 {
                    println!("Resut {}", x * y * z);
                    panic!();
                }
            }
        }
    }
    
}
