use std::fs;
use std::io::prelude::*;
use std::io;
use std::collections::HashMap;

struct Example {
    letter: char,
    min_limit: u32,
    max_limit: u32,
    password: String,
}

impl Example {
    fn is_valid(self) -> bool {
        let mut counter: HashMap<char, u32> = HashMap::new();
        for c in self.password.chars() {
            *counter.entry(c).or_insert(0) += 1;
        }
        let num = *counter.entry(self.letter).or_insert(0);
        num >= self.min_limit && num <= self.max_limit
    }
    fn is_valid_2(self) -> bool {
        let mut num: i32 = 0;
        let passwd = self.password.chars().collect::<Vec<char>>();
        num += ((self.min_limit as usize - 1) < passwd.len()
                && passwd[self.min_limit as usize - 1] == self.letter) as i32;
        num += ((self.max_limit as usize - 1) < passwd.len()
                 && passwd[self.max_limit as usize - 1] == self.letter) as i32;
        num == 1
    }
}

fn parse_single(s: &str) -> io::Result<Example> {
    // 1-3 b: cdefg
    let s = s.split(" ").collect::<Vec<&str>>();
    // 1-3
    let minmax = s[0].split("-").collect::<Vec<&str>>();
    let min = minmax[0].parse::<u32>().unwrap();
    let max = minmax[1].parse::<u32>().unwrap();
    // b:
    let letter = s[1].chars().nth(0).ok_or(std::io::ErrorKind::InvalidData)?;
    let passwd = s[2].to_string();

    let ex = Example{letter: letter, 
            min_limit: min,
            max_limit: max,
            password: passwd};
    Ok(ex)
}

fn parse_examples(input: String) -> io::Result<Vec<Example>> {
    let v = input.split("\n")
                 .map(parse_single)
                 .map(|x| x.unwrap())
                 .collect::<Vec<Example>>();
    Ok(v)
}

fn main() -> io::Result<()> {
    let raw_data = fs::read_to_string("data.txt").unwrap();
    let examples = parse_examples(raw_data)?;
    let mut num: u32 = 0;
    for e in examples {
        if e.is_valid_2() {
            num += 1;
        }
    }
    println!("Total num: {}", num);
    Ok(())
}
