use std::fs;
use std::io::prelude::*;
use std::io;
use std::collections::HashMap;
use std::collections::HashSet;
use std::num;
use std::num::ParseIntError;



pub struct ParserError {
    pub message: String,
}

impl From<std::num::ParseIntError> for ParserError {
    fn from(_: std::num::ParseIntError) -> ParserError {
        ParserError{message: "Invalid data type".to_string()}
    }
}

fn parse_single(s: &str) -> io::Result<HashMap<String, String>> {
    let mut ret: HashMap<String, String> = HashMap::new();
    for l in s.split("\n") {
        for kv in l.split(" ") {
            let tmp: Vec<&str> = kv.split(":").collect();
            if tmp.len() != 2 {
                println!("{}", kv);
                continue;
            }
            ret.insert(tmp[0].to_string(), tmp[1].to_string());
        }
    }
    Ok(ret)
}

fn parse_examples(input: String) -> io::Result<Vec<HashMap<String, String>>> {
    let v = input.split("\n\n")
                 .map(parse_single)
                 .map(|x| x.unwrap())
                 .collect::<Vec<HashMap<String, String>>>();
    Ok(v)
}

fn is_valid(ex: HashMap<String, String>) -> bool {
    let mut ret: bool  = true;
    // println!("ex: {}", ex["pid"]);
    if !(ex.len() == 8 || (ex.len() == 7 && !ex.contains_key("cid"))) {
        println!("num {}", ex.len());
        ret = false;
        return ret;
    }
    let byr = match ex["byr"].parse::<u32>() {
        Ok(x) => x,
        _ => 1919,
    };
    if byr < 1920 || byr > 2002 {
        println!("byr {}", byr);
        ret = false;
    }

    let iyr = match ex["iyr"].parse::<u32>() {
        Ok(x) => x,
        _ => 1919,
    };
    if iyr < 2010 || iyr > 2020 {
        println!("iyr {}", iyr);
        ret = false;
    }
    
    let eyr = match ex["eyr"].parse::<u32>() {
        Ok(x) => x,
        _ => 1919,
    };
    if eyr < 2010 || eyr > 2030 {
        println!("eyr {}", eyr);
        ret = false;
    }

    let hgt: &str = &ex["hgt"];
    if hgt.ends_with("cm") {
        let hgt_val = hgt[..hgt.len()-2].parse::<i32>().unwrap_or(0);
        if hgt_val > 193 || hgt_val < 150 {
            println!("hgt {}", hgt);
            ret = false;
        }
    } else if hgt.contains("in") {
        let hgt_val = hgt[..hgt.len()-2].parse::<i32>().unwrap_or(0);
        if hgt_val > 76 || hgt_val < 59 {
            println!("hgt {}", hgt);
            println!("hgt_val {}", hgt_val);
            ret = false;
        }
    } else {
        ret = false;
        println!("hgt {}", hgt);
    }
    let hcl: &str = &ex["hcl"];
    if hcl.starts_with("#")  {
        let hcl_val = u32::from_str_radix(hcl.trim_start_matches("#"), 16).unwrap_or(0xffffffff);
        if hcl_val > 0xffffff {
            println!("hcl {}", hcl);
            ret = false;
        }
    } else {
        println!("hcl {}", hcl);
        ret = false;
    }
    let ecl: &str = &ex["ecl"];
    let colors: HashSet<&'static str> =
    [ "amb", "blu", "brn", "gry", "grn", "hzl", "oth"].iter().cloned().collect();
    if !colors.contains(ecl) {
        println!("ecl {}", ecl);
        ret = false;
    }
    let pid: &str = &ex["pid"];
    let pid_val = match pid.parse::<i64>() {
        Ok(x) => x,
        _ => -1,
    };
    if pid.len() != 9 || pid_val < 0 {
        println!("pid {}", pid);
        ret = false;
    }

    ret
}

fn main() -> io::Result<()> {
    let raw_data = fs::read_to_string("data.txt").unwrap();
    let examples = parse_examples(raw_data)?;
    let mut ans: u32 = 0;
    for ex in examples {
        ans += is_valid(ex) as u32;
    }

    println!("Total num: {}", ans);
    Ok(())
}
