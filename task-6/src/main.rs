use std::fs;
use std::io::prelude::*;
use std::collections::HashSet;

fn func(s: &str) -> i32 {
    let mut set: HashSet<char> = HashSet::new();
    for l in s.split("\n") {
        for c in l.chars() {
            set.insert(c);
        }
    }
    set.len() as i32
}
fn func2(s: &str) -> i32 {
    let mut set_total: HashSet<char> = HashSet::new();
    let mut spl = s.split("\n");
    let mut set_total: HashSet<char> = spl.next().unwrap().chars().collect();
    for l in spl {
        let mut set: HashSet<char> = l.chars().collect();
        set_total = set_total.intersection(&set).cloned().collect();
        println!("{}", set_total.len());
    }
    println!("{} {}", s, set_total.len());
    set_total.len() as i32
}

fn input_parse(input: String) -> Vec<i32> {
    let v = input.split("\n\n")
                 .map(func2)
                 .collect::<Vec<i32>>();
    v
}

fn main() {
    let raw_data = fs::read_to_string("data.txt").unwrap();
    let data = input_parse(raw_data);
    println!("Ans: {}", data.iter().sum::<i32>());
}
